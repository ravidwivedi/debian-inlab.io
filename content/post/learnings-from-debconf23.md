---
title: "Learning from organizing an international DebConf23 in India"
date: 2023-09-22T00:30:00+05:30
draft: false
author: "Pirate Praveen"
tags: ["debian", "debconf23", "debian-india", "learnings", "international", "conference", "india"]
---

Some things we learned while organizing DebConf 23 at Kochi, sharing this in case some one organizing an international conference in future may find this useful.

* Visa type: Conference visa is strongly recommended, if someone is already in India for tourism they have to intimate FRRO (Foreigners Regional Registration Office) before the conference. 
  * e-visa can happen in 1-2 days but people applying at missions may need upto a week or longer to get the visa.
  * People coming on conference visa can combine tourism (but not the reverse).
  * For conference visa application, people will need these documents.
   1. Invitation from organizer (mandatory), 
   2. Political Clearance from Ministry of External Affairs (MEA) (mandatory) and 
   3. Security clearance from Ministry of Home Affairs (MHA)  (If required, see "Security clearance from MHA" section below).

* Political clearance from MEA: Close registrations for foreigners at least a month before the conference (the earlier the better) as you will need to share the full participants list including passport details with MEA for political clearance (this can take upto 2-3 weeks).
  * You need to contact Coordination department of MEA. Send email to SO (COORD) from https://videshapps.gov.in/videsh/divsion_data/342 You may call them for updates.
  * For participants from some countries, they may ask for their CV and profile (speaker or participant).
  * The request need to be in a letter head of a registered organization (signed and scanned).
  * It may be easier to collect the passport details along with registration form.

* Security clearance from MHA: If you have participants from countries that need security clearance from MHA (countries like Pakistan, Sudan and stateless persons/undefined nationality), you might have to close registrations much earlier as MHA clearance should be applied 60 days before the conference for private organizers and 30 days before for government institutions.
  * MEA clearance is a pre condition to MHA clearance.
  * If you have such participants or participants from countries with a bad bilateral relations, these clearances can take even longer or may never happen.
  * You can apply for MHA clearance from https://conference.mha.gov.in/

* Invitation Letter: They will need an invitation letter from organizers.
  * The invitation letter should contain who will be paying for their travel, food and accommodation.
  * Also personal details like passport number, country, home address, nationality.
  * They will also need a reference address when applying for e-Visa.

* You will need to intimate the FRRO Office before the conference.
  * You may get calls from local police asking for details about the conference. You may need to share the accommodation details and list of countries.

* For attendees: see https://debconf23.debconf.org/about/visas/


