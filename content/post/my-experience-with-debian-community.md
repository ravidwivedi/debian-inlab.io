---
title: "My Experience With Debian Community"
date: 2021-09-01T21:20:49+05:30
draft: false
author: "Pavit Kaur"
authorUrl: "https://pavitkaur05.github.io"
tags: ["debian", "GSoC"]
---

The highlight of my summer is that I got the opportunity to spend it with Debian Community, and I would like to share my wonderful experience with all. Before moving ahead, a little introduction about me. I am Pavit Kaur, an undergraduate student pursuing Bachelor of Computer Science and Engineering in India and was a Google Summer of Code 2021 student developer with Debian.

Starting from the very beginning, my journey with Debian commenced when the application period of Google Summer of Code begins. I chose one of the Debian Projects to apply. As this was my first time working in Open Source, I was both scared and excited when I started a conversation with mentors of the project regarding the project. To my surprise, I got complete guidance from the mentors [Antonio Terceiro](https://salsa.debian.org/terceiro) and [Paul Gevers](https://salsa.debian.org/elbrus) (both DDs),  even for the smallest of the queries that eventually help me to contribute to the project and finally getting accepted to the program. 

During the GSoC period, Antonio and Paul arranged for a weekly call for discussing project tasks and any issues I faced. The meetings were both fun and full of learning. The discussions were not just limited to the scope of the project but they even guided me to explore and learn more for contributing to the community.  

The other fascinating thing which happened during this period was that I attended and participated in DebConf21, which gave me insights into other Debian projects and activities. During DebConf, I also got the opportunity to meet and interact with the Debian-India Community members. I enjoyed the meet a lot and they all made me feel so welcome as if I was always part of them.

Throughout my time, I have experienced the commitment of Debian towards free software and saw how this policy allows for best practices and design patterns to develop and mature. My experience with the community has also made me realize the fun and learning opportunities associated with working open-source. I can say for sure that I am here to stay and would try to remain an active member of the Debian Community :smiley:
